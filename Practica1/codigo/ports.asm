            list	p=16F84A
            include	p16f84a.inc
            org			0x00		;Vector de Reset
			goto		InitPortA	;Salto incondicional al principio del programa
			org			0x05

;Initializing PORTA (5 bit register)
InitPortA   bcf     STATUS,RP0
            clrf    PORTA
            bsf     STATUS,RP0
            movlw   0x1F            ; all pins (4:0) set as inputs
            movwf   TRISA

;Initializing PORTB (8 bit register)
            bcf     STATUS,RP0
            clrf    PORTB
            bsf     STATUS,RP0
            movlw   0x00            ; all pins (7:0) set as outputs
            movwf   TRISB
            clrw                    ; clear working register before the loop
;Check PORTA for 11111 (0x1F)
loop        bcf     STATUS,RP0      ; switches back to Bank 0
            movf    PORTA,0         ; moves PORTA to W
            sublw   0x1F            ; 11111 - W
            btfsc   STATUS,Z        ; if Z = 1, PORTA = 0x1F
            goto    Stop            ; puts the program to sleep
            movf    PORTA,0			; moves PORTA to W
            movwf   PORTB			; moves  W to PORTB
            goto    loop			

Stop        sleep
            end
