;Para realizar la comparacion restamos los dos numeros (A-B)
;Si al realizar la resta los dos numeros son iguales el resultado sera 0.
;De esa forma se activa el bit Z del registro de Estado.
;Si al realizar la resta (suma del complemento a 2 de B) se produce un bit
;de acarreo el resultado es positivo(A>B).
;Si no se produce bit de acarreo el resultado es negativo (A<B).

			list		p=16F84A
			include		p16f84a.inc	;case sensitive in linux
NumA		equ			0x0c		;variable numero A
NumB		equ			0x0d		;variable numero B
Mayor		equ			0x0e		;variable que almacena el numero mayor
			org			0x00		;Vector de Reset
			goto		Inicio		;Salto incondicional al principio del programa
			org			0x05		;Vector de interrupción
Inicio		movf		NumB,W		;NumB->W(Acumulador)
			subwf		NumA,W		;A-W->W
			btfsc		STATUS,Z	;Check Z Bit Status & skip if clear
			goto		A_igual_B	;Si STATUS,Z es 1 va a A_igual_B
			btfsc		STATUS,C	;Bit de acarreo del registro de Estado
			goto		A_mayor_B   ;Si hay acarreo, ve a A_mayor_B
A_menor_B	movf        NumB,W		;Mueve NumB a W
			movwf		Mayor		;Mueve W a Mayor
			goto		Stop
A_mayor_B	movf		NumA,W		;Mueve NumA a W
			movwf		Mayor       ;Mueve W a Mayor
            goto		Stop
A_igual_B	clrf		Mayor		;Pone a cero el resultado
Stop		nop						;Pone el breakpoint de parada
			end						;Fin del programa

