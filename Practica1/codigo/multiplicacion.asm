;Multiplicar 2 números de 8 bits usando algoritmo sumas parciales
;2015-05-17 Eduardo Molina
;INPUT: NumA en 0C, NumB en 0D
;OUTPUT: DH en 0E, DL en 0F
            list		p=16F84A
			include		p16f84a.inc	;case sensitive in linux
NumA		equ			0x0c		;numero A (Multiplicando)
NumB		equ			0x0d		;numero B (Multiplicador)
DH          equ			0x0e		;Acumulador byte HI
DL          equ         0x0f        ;Acumulador byte LO
Count       equ         0x10        ;Contador
			org			0x00		;Vector de Reset
			goto		MAIN		;Salto incondicional al principio del programa
			org			0x05		;Vector de interrupción
MAIN        call        Mult        ;Llama a la subrutina multiplicar
            goto        Stop        ;Va al fin del programa
;============================================================================
;subrutina Mult
;============================================================================
Mult        movlw       d'8'
            movwf       Count       ;Inicializa el contador a 8
            clrf        DH          ;Borra el contenido de DH
            clrf        DL          ;Borra el contenido de DL
            movf        NumB,0      ;Mueve NumB a W
            movwf       DL          ;Mueve NumB a DL
S_Loop      btfsc       DL,0        ;Comprueba el contenido del bit 0 de DL
            goto        Suma
E_Loop      bcf         STATUS,C    ;Anula el bit de Carry
            rrf         DH,1        ;Rota a derechas DH
            rrf         DL,1        ;Rota a derechas DL
            decfsz      Count,1     ;Resta uno a count
            goto        S_Loop      ;Si count es cero no se ejecuta
            goto        End_sub     ;Salir de la subrutina
Suma        movf        NumA,0      ;Mueve el número A a W
            addwf       DH,1        ;Suma W(NumA) a DH (Salvo si DL[0]==0)
            goto        E_Loop      ;Vuelve al cierre del Loop
End_sub     return                  ;Vuelta a MAIN
;==============================================================================
Stop        nop
            end


