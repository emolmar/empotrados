;Multiplicar dos números introducidos por el portA y mostrarlos en PortB
;2015-05-25 Eduardo Molina

            list		p=16F84A
			include		p16f84a.inc	;case sensitive in linux
NumA		equ			0x0c		;numero A (Multiplicando)
NumB		equ			0x0d		;numero B (Multiplicador)
DH          equ			0x0e		;Acumulador byte HI
DL          equ         0x0f        ;Acumulador byte LO
Count       equ         0x10        ;Contador
			org			0x00		;Vector de Reset
			goto		MAIN		;Salto incondicional al principio del programa
MAIN        clrf        NumA
            clrf        NumB
            clrf        DH
            clrf        DL
            bcf         STATUS,RP0  ;Initializing PORTA (5 bit register)
            clrf        PORTA
            bsf         STATUS,RP0
            movlw       0x1F        ;all pins (4:0) set as inputs
            movwf       TRISA
            bcf         STATUS,RP0  ;Initializing PORTB (8 bit register)
            clrf        PORTB
            bsf         STATUS,RP0
            movlw       0x00        ;all pins (7:0) set as outputs
            movwf       TRISB
            bcf         STATUS,RP0
;============================================================================
;Lectura de puertos
;============================================================================
ReadPort    btfss       PORTA,4     ;Comprobamos el valor del bit4 en PORTA
            goto        MoveToA     ;Si el bit es cero, vamos a MoveToA
            movf        PORTA,0     ;Si el bit es 1, hay que mover a NumB
            andlw       0x0F        ;Borramos el bit 4
            movwf       NumB
            movf        NumA,0
            btfsc       STATUS,Z    ;Comprobamos si numA tiene valor no cero
            goto        ReadPort
            goto        Mult_p
MoveToA     movf        PORTA,0
            andlw       0x0F
            movwf       NumA
            movf        NumB,0
            btfsc       STATUS,Z    ;Comprobamos si NumB tiene un valor no cero
            goto        ReadPort
Mult_p      call        Mult        ;Llama a la subrutina multiplicar
            movf        DL,0        ;mueve DL a W
            movwf       PORTB       ;mueve W a PORTB
            goto        Stop        ;Va al fin del programa
;============================================================================
;subrutina Mult
;============================================================================
Mult        movlw       d'8'
            movwf       Count       ;Inicializa el contador a 8
            clrf        DH          ;Borra el contenido de DH
            clrf        DL          ;Borra el contenido de DL
            movf        NumB,0      ;Mueve NumB a W
            movwf       DL          ;Mueve NumB a DL
S_Loop      btfsc       DL,0        ;Comprueba el contenido del bit 0 de DL
            goto        Suma
E_Loop      bcf         STATUS,C    ;Anula el bit de Carry
            rrf         DH,1        ;Rota a derechas DH
            rrf         DL,1        ;Rota a derechas DL
            decfsz      Count,1     ;Resta uno a count
            goto        S_Loop      ;Si count es cero no se ejecuta
            goto        End_sub     ;Salir de la subrutina
Suma        movf        NumA,0      ;Mueve el número A a W
            addwf       DH,1        ;Suma W(NumA) a DH (Salvo si DL[0]==0)
            goto        E_Loop      ;Vuelve al cierre del Loop
End_sub     return                  ;Vuelta a MAIN
;==============================================================================
Stop        nop
            end


